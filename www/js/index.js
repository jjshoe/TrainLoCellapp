/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        var settings = new Settings();
        var gpsBubbles = new GpsBubbles();

        // Listen for log level changes
        $('#select-log-level').change(function() {
            Logger.setLogLevel($(this).val());
	});

        // Load in the settings
        settings.loadSettings();

        // Set the username, password, and server if we have one
        $('#username').val(settings.username);
        $('#password').val(settings.password);
        $('#server').val(settings.server);
        $('#port').val(settings.port);

        gpsBubbles.loadBubbles({username: settings.username, password: settings.password, server: settings.server, port: settings.port});

        // Listen for settings changes, and write them out
        $("#saveSettings").on("click", function(e) {
            settings.saveSettings({username: $('#username').val(), password: $('#password').val(), server: $('#server').val(), port: $('#port').val()});
            gpsBubbles.loadBubbles({username: settings.username, password: settings.password, server: settings.server, port: settings.port});
        });

        // When we do the calculations, we should iterate ALL Bubbles, and send a notice that we are closest to bubble X only if we where last closer to another bubble
        var options = {timeout: 5000, enableHighAccuracy: true, maximumAge: 3000};
        navigator.geolocation.watchPosition(function (position) {gpsBubbles.shareLocation({settings: settings, position: position})}, function (error) {Logger.critical(error.code)}, options);
    }
};

app.initialize();
