var Settings = function Settings() {
    this.username = null;
    this.password = null;
    this.server = null;
    this.port = null;
};

Settings.prototype = { 
    loadSettings: function loadSettings() {
        this.username = window.localStorage.getItem('username');
        this.password = window.localStorage.getItem('password');
        this.server = window.localStorage.getItem('server');
        this.port = window.localStorage.getItem('port');
    },

    saveSettings: function saveSettings(settings) {
        window.localStorage.setItem('username', settings.username);
        window.localStorage.setItem('password', settings.password);
        window.localStorage.setItem('server', settings.server);
        window.localStorage.setItem('port', settings.port);

        this.loadSettings();
    },
};
