var Logger = function Logger() {
    this.loglevel = 1;
};

Logger.critical = function(message) {
    if (this.loglevel >= 1) {
        this.logMessage('Critical: ' + message);
    }
};


Logger.verbose = function(message) {
    if (this.loglevel >= 2) {
        this.logMessage('Verbose: ' + message);
    }
};

Logger.debug = function(message) {
    if (this.loglevel >= 3) {
        this.logMessage('Debug: ' + message);
    }
};

Logger.logMessage = function(message) {
       // If we have a ton of messages start removing old ones
       if ($('#loglist li').length > 25) {
          $('#loglist li:visible:first').remove();
       }

       // Add the new log message
       $('#loglist').append('<li>' + message + '</li>');

};

Logger.setLogLevel = function setLogLevel(level) {
    console.log('log level: ' + level);
    this.loglevel = level;
};
