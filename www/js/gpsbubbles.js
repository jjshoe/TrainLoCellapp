var GpsBubbles = function GpsBubbles() {
    this.bubbles = []
    this.closestBubbleId = null
    this.closestBubbleName = null
};

GpsBubbles.prototype = {
    // Make an AJAX call off to get the bubbles 
    loadBubbles: function loadBubbles(settings) {
        $.get('http://' + settings.server + ':' + settings.port + '/train_tracks/bubbles/' + settings.username + '/' + settings.password, function (data) {
            GpsBubbles.bubbles = data;
        }, 'json')
        .error(function (obj, string, exception) {
            Logger.critical(obj);
            Logger.critical(string);
            Logger.critical(exception);
        });
    },

    closestBubble: function closestBubble(position) {
        var currentDistance = null;
        var currentBubbleId = null; 
        var currentBubbleName = null; 

        Logger.debug(position.coords.longitude);
        Logger.debug(position.coords.latitude);
        Logger.debug(position.coords.accuracy);

	if (GpsBubbles.bubbles) {
            var len = GpsBubbles.bubbles.length; 

            for (var i=0; i<len; ++i) {
                var lon1 = position.coords.longitude;
                var lat1 = position.coords.latitude;

                var lon2 = GpsBubbles.bubbles[i].coords.longitude;
                var lat2 = GpsBubbles.bubbles[i].coords.latitude;

                var R = 6371; // Radius of the earth in km
                var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
                var dLon = this.deg2rad(lon2-lon1); 
                var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * Math.sin(dLon/2) * Math.sin(dLon/2); 
                var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
                var d = R * c; // Distance in km

                if (!currentDistance || d < currentDistance) {
                    currentBubbleId = GpsBubbles.bubbles[i].id;
                    currentBubbleName = GpsBubbles.bubbles[i].name;
                    currentDistance = d;
                }
            }

            Logger.verbose('The streetcar is near the ' + currentBubbleName);

            GpsBubbles.closestBubbleId = currentBubbleId;
            GpsBubbles.closestBubbleName = currentBubbleName;
        }
    },

    shareLocation: function shareLocation(args) {
        var settings = args.settings;
        var position = args.position;
        var oldBubbleId = GpsBubbles.closestBubbleId

        this.closestBubble(position);

        if (oldBubbleId != GpsBubbles.closestBubbleId) {
            $.get('http://' + settings.server + ':' + settings.port + '/train_tracks/bubble/' + settings.username + '/' + settings.password + '/' + GpsBubbles.closestBubbleId, function () {
            })
            .error(function (obj, string, exception) {
                Logger.critical(obj);
                Logger.critical(string);
                Logger.critical(exception);
            });
        }
    },

    deg2rad: function deg2rad(deg) {
        return deg * (Math.PI/180)
    }
};
